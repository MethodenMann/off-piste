from importlib import reload
from runpy import _run_module_as_main
from unittest.mock import patch

import gnar_gear

from .constants import ENVIRONMENT


class TestMain:

    def test_init(self, monkeypatch):
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('argparse.ArgumentParser.parse_args', lambda x: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('bjoern._default_instance',
                            (type('mock_sock', (object,), {'family': None, 'close': lambda: None}), None))
        monkeypatch.setattr('bjoern.server_run', lambda *a, **kw: None)
        with patch('bjoern.listen') as mock_run:
            reload(gnar_gear)
            _run_module_as_main('off-piste.app.main')
            assert mock_run.call_count == 1
